 <?php
	//Get the resto name
	$num_resto=$_POST["NameResto"];
	
	//Prepare the URL
	$url = "http://localhost/webservice/get_REST.php?function=getProduct&arg=";
	$url .= $num_resto;		//Add the resto name to the URL
	
	//Initialisation of the cURL
	$curl = curl_init($url);
	
	//Define the cURL transmission option 
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	
	//Execute the cURL
	$curl_response = curl_exec($curl);
	
	//Close the cURL
	curl_close($curl);
	
	//decode the cURL reponse
	$decoded = json_decode($curl_response);
	
	//Start the reponse html
	$html = '
	<!doctype html>
	<HTML lang="fr">
	<HEAD>
	<link rel="stylesheet" href="style.css">
	  <META charset="utf-8">
	  <TITLE>Commande</TITLE>
	</HEAD>
		  <BODY>';
	if( $decoded !== null && $decoded !=="ERR_REST_API"){//Check no error
		//Put the dropbox product name in the html variable
		$html .='<div class = "text">
		<FORM method="post" action="getGIF.php" >
			Select a product:
			<SELECT class="dropdown" name="NameProduct" size="1" >';
		foreach($decoded as $rows)//browse the array cURL reponse
			foreach($rows as $row)//browse the array cURL reponse 2nd Dim
				$html .= '<OPTION>'.$row;//Get the product name
				
		//Put the end of the select tag
		$html .= '</SELECT>
			<button class="button" type="submit">Commander le produit</button>
		</FORM>
		</div>';
	}else $html .= '<DIV class="Err">UNE ERREUR S\'EST PRODUITE</DIV>';
	
	//Put the end tag in the html variable
	$html .= '</BODY></HTML>';
	
	//return the html page
	echo $html;