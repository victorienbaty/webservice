-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mar. 23 oct. 2018 à 16:15
-- Version du serveur :  5.7.23
-- Version de PHP :  7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `webservice`
--

-- --------------------------------------------------------

--
-- Structure de la table `gif`
--

DROP TABLE IF EXISTS `gif`;
CREATE TABLE IF NOT EXISTS `gif` (
  `ID_GIF` int(11) NOT NULL AUTO_INCREMENT,
  `GIF_url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID_GIF`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `gif`
--

INSERT INTO `gif` (`ID_GIF`, `GIF_url`) VALUES
(1, 'https://media.giphy.com/media/Rkis28kMJd1aE/giphy.gif '),
(2, 'https://media.giphy.com/media/11jXkZ1RR8d5YI/giphy.gif'),
(3, 'https://media.giphy.com/media/bPShx901m0HHG/giphy.gif'),
(4, 'https://media.giphy.com/media/l44Qqz6gO6JiVV3pu/giphy.gif'),
(5, 'https://media.giphy.com/media/11dgYWwHnEFomQ/giphy.gif'),
(6, 'https://media.giphy.com/media/10kxE34bJPaUO4/giphy.gif');

-- --------------------------------------------------------

--
-- Structure de la table `gif_product`
--

DROP TABLE IF EXISTS `gif_product`;
CREATE TABLE IF NOT EXISTS `gif_product` (
  `ID_GIF` int(11) NOT NULL,
  `ID_Product` int(11) NOT NULL,
  PRIMARY KEY (`ID_GIF`,`ID_Product`),
  KEY `ID_Product` (`ID_Product`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `gif_product`
--

INSERT INTO `gif_product` (`ID_GIF`, `ID_Product`) VALUES
(2, 1),
(3, 2),
(4, 3),
(5, 4),
(6, 5);

-- --------------------------------------------------------

--
-- Structure de la table `product`
--

DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `ID_product` int(11) NOT NULL AUTO_INCREMENT,
  `Product_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID_product`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `product`
--

INSERT INTO `product` (`ID_product`, `Product_name`) VALUES
(1, 'carotte'),
(2, 'patate'),
(3, 'ordinateur'),
(4, 'chat'),
(5, 'pizza');

-- --------------------------------------------------------

--
-- Structure de la table `product_resto`
--

DROP TABLE IF EXISTS `product_resto`;
CREATE TABLE IF NOT EXISTS `product_resto` (
  `ID_Resto` int(11) NOT NULL,
  `ID_Product` int(11) NOT NULL,
  PRIMARY KEY (`ID_Resto`,`ID_Product`),
  KEY `ID_Product` (`ID_Product`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `product_resto`
--

INSERT INTO `product_resto` (`ID_Resto`, `ID_Product`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(2, 1),
(2, 2),
(2, 5),
(3, 1),
(3, 2),
(3, 3),
(4, 1),
(4, 2),
(4, 4),
(4, 5);

-- --------------------------------------------------------

--
-- Structure de la table `resto`
--

DROP TABLE IF EXISTS `resto`;
CREATE TABLE IF NOT EXISTS `resto` (
  `ID_Resto` int(11) NOT NULL AUTO_INCREMENT,
  `Resto_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID_Resto`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `resto`
--

INSERT INTO `resto` (`ID_Resto`, `Resto_name`) VALUES
(1, 'resto1'),
(2, 'resto2'),
(3, 'resto3'),
(4, 'resto4');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `User_name` varchar(255) NOT NULL,
  `mdp` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`User_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`User_name`, `mdp`) VALUES
('resto1', '791219d9a7b5b109f3d4ae91f39a5b34'),
('resto2', '9e3a5485ed70994b5daa661f2a47d31f'),
('resto3', '8bbab074ad07006552adff776b223ed1'),
('resto4', '81a9de81576d062a15753439ee8ad76f');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
