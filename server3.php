<?php
ini_set('soap.wsdl_cache_enabled', '0');
require_once ('C:\xampp\htdocs\Webservice\lib\nusoap.php');
ob_start();
//using soap_server to create server object
$server = new soap_server;
$server->configureWSDL('get','https://127.0.0.1/webservice/server3.php'); //emplacement du script sur le serveur web
$server->wsdl->schenaTargetNamespace='http://soapinterop.org/xsd/';
//register a function that works on server
$server->register('update_db', array('file'=>'xsd:string'),array('result'=>'xsd:string'),'http://soapinterop.org/'); 
//le serveur enregistre la fonction get_file ainsi que ses arguments
$server->soap_defencoding = 'UTF-8';
$server->encode_utf8 = true;
$server->decode_utf8 = FALSE;

function update_db($file)
{
    $filename='C:\xampp\htdocs\Webservice\test.csv';
    $handle=fopen($filename,"rb");  
    fwrite($handle,$file); //lecture du fichier demandé
    $rep=fread($handle,filesize($filename));
    fclose($handle);
    $rep_array=explode(";",$rep);
    $array_size=sizeof($rep_array);
    $rep2="";
    for($i=0;$i<$array_size;$i++)
    {
     $rep2=$rep2.$rep_array[$i];
    }
    return $rep2;
}

// create HTTP listener
$server->service(file_get_contents("php://input")); //Attend un appel d'un webservice client
exit();
?>