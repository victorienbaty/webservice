 <?php
//BDD lib
include "dbconnect.php";

//----------Get product by resto name
function getProduct($resto_name){	
	//New BDD object
	$DBase = new Connect;
	
	//Connection at the BDD
	$db = $DBase->connexion();
	
	if($db)//If connexion don't fail
	{
		//Prepare the SQL request
		$request= $db->prepare("SELECT Product_name from resto RIGHT JOIN product_resto on product_resto.ID_resto = resto.ID_resto RIGHT JOIN product ON product.ID_product = product_resto.ID_product WHERE resto_name='".$resto_name."'");
		
		//Execute the SQL request
		$request->execute();
		
		//Get the SQL return
		$result = $request->fetchAll(PDO::FETCH_OBJ);
		
		//Return result variable
		return $result;
	}
	else return "ERR_REST_API";//If the connexion fail, return the error chain
}

//----------Get GIF by the product name
function getGIF($product_name){
	//New BDD object
	$DBase = new Connect;
	//Connection at the BDD
	$db = $DBase->connexion();
	
	if($db)//If connexion don't fail
	{
		//Prepare the SQL request
		$request= $db->prepare("SELECT GIF_url FROM product RIGHT JOIN gif_product ON gif_product.ID_Product = product.ID_Product RIGHT JOIN gif ON gif_product.ID_GIF = gif.ID_GIF WHERE Product_name='".$product_name."'");
		
		//Execute the SQL request
		$request->execute();
		
		//Get the SQL return
		$result = $request->fetchAll(PDO::FETCH_OBJ);
		
		//Return result variable
		return $result;
	}
	else return "ERR_REST_API";//If the connexion fail, return the error chain
}

//----------Get the GIF error by the id of this GIF
function getGIFErr($ID_GIF){
	//New BDD object
	$DBase = new Connect;
	
	//Connection at the BDD
	$db = $DBase->connexion();
	
	if($db)//If connexion don't fail
	{
		//Prepare the SQL request
	    $request= $db->prepare("SELECT GIF_url FROM gif WHERE ID_GIF='".$ID_GIF."'");
		
		//Execute the SQL request
		$request->execute();
		
		//Get the SQL return
		$result = $request->fetchAll(PDO::FETCH_OBJ);
		
		//Return result variable
		return $result;
	}
	else return "ERR_REST_API";//If the connexion fail, return the error chain
}



if($_GET['arg']!='' && $_GET['function']!='' ){	//If argument
	$value = @call_user_func($_GET['function'], $_GET['arg']); 	//Call the fonction
	if($value === null)
		$value = "ERR_REST_API";
 echo json_encode($value);	//encode the variable value and return it
}else{	//If no argument
	if($_GET['function'] === "getGIF")
		$value = getGIFErr(1); 	//Get the GIF error
	else $value = "ERR_REST_API";
 echo json_encode($value);	//encode the variable value and return it
}	