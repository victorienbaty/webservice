 <?php
	//Get the product name
	$num_produit=$_POST["NameProduct"];
	
	//Prepare the URL
	$url = "http://localhost/webservice/get_REST.php?function=getGIF&arg=";
	$url .= $num_produit;	//Add the product name to the URL
	
	//Initialisation of the cURL
	$curl = curl_init($url);
	
	//Define the cURL transmission option 
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	
	//Execute the cURL
	$curl_response = curl_exec($curl);
	
	//Close the cURL
	curl_close($curl);
	
	//decode the cURL reponse
	$decoded = json_decode($curl_response);
	
	//Start the reponse html
	$html = '<!doctype html>
	<html lang="fr">
	<head>
		<link rel="stylesheet" href="style.css">
		<meta charset="utf-8">
		<title>GIF</title>
	</head><body>';
	if( $decoded !== null && $decoded !=="ERR_REST_API"){//Check no error
		//Put the img tag in the html variable
		$html .='<img style="text-align:center" src="';
		foreach($decoded as $rows)//browse the array cURL reponse
			foreach($rows as $row)//browse the array cURL reponse 2nd Dim
				$html .= $row;//Get the url GIF
		
		//Put the end of the img tag
		$html .= '" width="800" height="800" alt="GIF">';
	}else $html.= '<DIV class="Err">UNE ERREUR S\'EST PRODUITE</DIV>';//Put the DIV error if $decoded error
	
	//Put the end tag in the html variable
	$html .='</body></html>';
	
	//return the html page
	echo $html;